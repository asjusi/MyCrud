﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyCrud.Controllers
{
    public class ServicosController : Controller
    {
        public ActionResult Index(string msn, string tipo)
        {
            ViewBag.msn = msn;
            ViewBag.tipo = tipo;
            MyCrudEntities entity = new MyCrudEntities();
            List<SERVICO> objs = entity.SERVICO.ToList();
            return View(objs);
        }

        public ActionResult Novo()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Criar(SERVICO obj)
        {
            try
            {
                MyCrudEntities entity = new MyCrudEntities();

                entity.SERVICO.Add(obj);
                entity.SaveChanges();

                return RedirectToAction("Index", new { msn = "Servico criado com sucesso!", tipo = "success" });
            }
            catch (Exception)
            {
                return RedirectToAction("Index", new { msn = "Falha ao criar!", tipo = "danger" });
            }
        }

        public ActionResult Editar(int Id)
        {
            MyCrudEntities entity = new MyCrudEntities();
            SERVICO obj = (from d in entity.SERVICO where d.ID == Id select d).FirstOrDefault();
            return View(obj);
        }
        [HttpPost]
        public ActionResult Salvar(SERVICO obj)
        {
            try
            {
                MyCrudEntities entity = new MyCrudEntities();
                entity.SERVICO.Attach(obj);
                entity.Entry(obj).State = System.Data.EntityState.Modified;
                entity.SaveChanges();
                return RedirectToAction("Index", new { msn = "Servico salvado com sucesso!", tipo = "success" });
            }
            catch (Exception)
            {
                return RedirectToAction("Index", new { msn = "Falha ao salvar!", tipo = "danger" });
            }
        }
        public ActionResult Detalhes(int Id)
        {
            MyCrudEntities entity = new MyCrudEntities();
            SERVICO obj = (from d in entity.SERVICO where d.ID == Id select d).FirstOrDefault();
            return View(obj);
        }

        public ActionResult Excluir(int Id)
        {
            MyCrudEntities entity = new MyCrudEntities();
            SERVICO obj = (from d in entity.SERVICO where d.ID == Id select d).FirstOrDefault();
            return View(obj);
        }
        [HttpPost]
        public ActionResult ConfirmarExcluir(SERVICO obj)
        {
            try
            {

                MyCrudEntities entity = new MyCrudEntities();
                SERVICO _obj = (from d in entity.SERVICO where d.ID == obj.ID select d).FirstOrDefault();
                if (_obj.OS_SERVICO.Count() > 0)
                    return RedirectToAction("Index", new { msn = "Voce nao pode excluir esse servico, pois ele possui Ordem de Servico!", tipo = "danger" });

                entity = new MyCrudEntities();
                entity.SERVICO.Attach(obj);
                entity.Entry(obj).State = System.Data.EntityState.Deleted;
                entity.SaveChanges();
                return RedirectToAction("Index", new { msn = "Servico excluido com sucesso!", tipo = "success" });
            }
            catch (Exception e)
            {
                return RedirectToAction("Index", new { msn = "Falha na exclusao!", tipo = "danger" });
            }
        }
    }
}
