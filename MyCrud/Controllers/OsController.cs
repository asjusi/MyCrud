﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyCrud;

namespace MyCrud.Controllers
{
    public class OsController : Controller
    {
        public ActionResult Index(string msn, string tipo)
        {
            ViewBag.msn = msn;
            ViewBag.tipo = tipo;
            MyCrudEntities entity = new MyCrudEntities();
            List<OS> objs = entity.OS.ToList();
            return View(objs);
        }

        public ActionResult Novo()
        {
            MyCrudEntities entity = new MyCrudEntities();
            @ViewBag.servicos = entity.SERVICO.ToList();
            OS os = (OS)Session["OS"];
            return View(os);
        }
        public ActionResult AdicionarServico(int Id)
        {
            OS_SERVICO obj = new OS_SERVICO();
            MyCrudEntities entity = new MyCrudEntities();

            obj.SERVICO = (from d in entity.SERVICO where d.ID == Id select d).FirstOrDefault();
            OS os = (OS)Session["OS"];

            if (os == null)
                os = new OS();
            if (obj != null)
                os.OS_SERVICO.Add(obj);
            Session["OS"] = os;
            @ViewBag.servicos = entity.SERVICO.ToList();
            return RedirectToAction("Novo");
        }
        [HttpPost]
        public ActionResult Criar(OS obj)
        {
            try
            {
                MyCrudEntities entity = new MyCrudEntities();
                entity.OS.Add(obj);
                entity.SaveChanges();

                OS os = (OS)Session["OS"];

                foreach (OS_SERVICO item in os.OS_SERVICO)
                    entity.OS_SERVICO.Add(new OS_SERVICO() { ID_SERVICO = item.SERVICO.ID, ID_OS = obj.ID });
                entity.SaveChanges();
                Session["OS"] = null;
                return RedirectToAction("Index", new { msn = "Ordem de servico criada com sucesso!", tipo = "success" });
            }
            catch (Exception)
            {
                return RedirectToAction("Index", new { msn = "Falha ao criar ordem de servico!", tipo = "danger" });
            }
        }
        public ActionResult Detalhes(int Id)
        {
            MyCrudEntities entity = new MyCrudEntities();
            OS obj = (from d in entity.OS where d.ID == Id select d).FirstOrDefault();
            obj.OS_SERVICO = (from x in entity.OS_SERVICO where x.ID_OS == Id select x).ToList();
            return View(obj);
        }
    }
}
